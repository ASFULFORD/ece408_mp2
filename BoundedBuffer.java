//class BoundedBuffer
//This class implements the synchronization methods to be used in 
//the bounded buffer problem 

public class BoundedBuffer
{
   //MP2 create any variables you need
   private char[] buffer;
   private Semaphore full;
   private Semaphore empty;
   private Semaphore mutex;
   private int in;
   private int out;
   private int counter;
   private int buffer_size;
   //BoundedBuffer
   //constructor:  initialize any variables that are needed for a bounded 
   //buffer of size "size"
   public BoundedBuffer(int size)
   {
      buffer      = new char[size];
      full        = new Semaphore("full", 0);
      empty       = new Semaphore("empty", size);
      mutex       = new Semaphore("mutex", 1);
      in          = 0;
      out         = 0;
      counter     = 0;
      buffer_size = size;
   }

   //produce()
   //produces a character c.  If the buffer is full, wait for an empty
   //slot
   public void produce(char c)
   {
     //MP2
     char nextProduced = c;
     
       empty.P();
       mutex.P();
       ++counter;
       buffer[in] = nextProduced;
       in = (in + 1) % buffer_size;
       mutex.V();
       full.V();
     
   }

   //consume()
   //consumes a character.  If the buffer is empty, wait for a producer.
   //use method SynchTest.addToOutputString(c) upon consuming a character. 
   //This is used to test your implementation.
   public void consume()
   {
     //MP2
     //make sure you change the following line accordingly
     char nextConsumed;
     
       full.P();
       mutex.P();
       --counter;
       nextConsumed = buffer[out];
       out = (out + 1) % buffer_size;
       SynchTest.addToOutputString(nextConsumed);
       mutex.V();
       empty.V();
     
   }
}
