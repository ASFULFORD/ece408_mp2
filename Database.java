//class Database
//This class implements the synchronization methods to be used in 
//the readers writers problem
public class Database
{
   //MP2 create any variables that you need for implementation of the methods
   //of this class
   private Semaphore readcount_mutex, writecount_mutex, read_mutex, write_mutex, enterreader_mutex;
   private int readcount, writecount;
   //Database
   //Initializes Database variables
   public Database()
   {
      //MP2
      readcount_mutex   = new Semaphore("readcount_mutex", 1);
      writecount_mutex  = new Semaphore("writecount_mutex", 1);
      read_mutex        = new Semaphore("read_mutex", 1);
      write_mutex       = new Semaphore("write_mutex", 1);
      enterreader_mutex = new Semaphore("enterreader_mutex", 1);
      readcount = 0;
      writecount = 0;
   }

   //napping()
   //this is called when a reader or writer wants to go to sleep and when 
   //a reader or writer is doing its work.
   //Do not change for MP2
   public static void napping()
   {
      Alarm ac = new Alarm(20);  
   }

   //startRead
   //this function should block any reader that wants to read if there 
   //is a writer that is currently writing.
   //it returns the number of readers currently reading including the
   //new reader.
   public int startRead()
   {
      //MP2
      enterreader_mutex.P();
      read_mutex.P();
      readcount_mutex.P();
      readcount++;
      if(readcount == 1)
         write_mutex.P();
      readcount_mutex.V();
      read_mutex.V();
      enterreader_mutex.V();
      return readcount;
   }

   //endRead()
   //This function is called by a reader that has finished reading from the 
   //database.  It returns the current number of readers excluding the one who
   //just finished.
   public int endRead()
   {
      //MP2
      readcount_mutex.P();
      readcount--;
      if(readcount == 0)
         write_mutex.V();
      readcount_mutex.V();
      return readcount;
   }

   //startWrite()
   //This function should allow only one writer at a time into the Database
   //and block the writer if anyone else is accessing the database for read 
   //or write.
   public void startWrite()
   {
      //MP2
      writecount_mutex.P();
      writecount++;
      if(writecount == 1)
         read_mutex.P();
      writecount_mutex.V();
      write_mutex.P();
   }
   
   //endWrite()
   //signal that a writer is done writing and the database can now be accessed
   //by someone who is waiting to read or write.
   public void endWrite()
   {
      //MP2
      write_mutex.V();
      writecount_mutex.P();
      writecount--;
      if(writecount == 0)
         read_mutex.V();
      writecount_mutex.V();
   }
}